from dataStream import *
from flask import Flask, Response
from flask_cors import CORS


app = Flask(__name__)
CORS(app)


@app.route("/")
def hello():
    stream = RandomDealData()
    InstrumentList = stream.createInstrumentList()
    data = stream.createRandomData(InstrumentList)
    return data


@app.route('/streamTest')
def stream():
    def eventStream():
        while True:
            # wait for source data to be available, then push it
            yield 'data:{}\n\n'.format(get_data())

    return Response(eventStream(), mimetype="text/event-stream")


def get_data():
    """this could be any function that blocks until data is ready"""
    stream = RandomDealData()
    InstrumentList = stream.createInstrumentList()
    data = stream.createRandomData(InstrumentList)
    return data


def bootapp():
    app.run(port=8080, threaded=True, host='0.0.0.0')


if __name__ == '__main__':
    bootapp()


