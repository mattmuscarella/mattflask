#DAO
docker build -t dao ./ 
docker run -dit --name dao dao
docker exec -it dao /bin/bash

#DATAGEN
docker build -t datagen ./ 
docker run -p 8090:8090 -dit --name datagen datagen
docker exec -it datagen /bin/bash

#MYSQL
docker run  -p 3306:3306 --name mysqlserver -e MYSQL_ROOT_PASSWORD=ppp -d mysql:latest


docker exec -it mysqlserver /bin/bash
mysql -u root -p

cat deals.sql | docker exec -i mysqlserver /usr/bin/mysql -u root --password=ppp deals

mysql -u root -p deals < sqldump.sql