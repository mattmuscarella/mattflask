import mysql.connector
import json
from sseclient import SSEClient


class InsertDAO:
    def __init__(self):
        self.mydb = mysql.connector.connect(  # DB port
            host="mysql.tigerdeals.svc.cluster.local",
            user="root",
            passwd="ppp",
            database="deals")
        self.myCursor = self.mydb.cursor()
        self.table = 'deals.deal_data'

    def insert(self, columns, placeholders):
        sql = "INSERT INTO {0} ({1}) VALUES ({2});".format(self.table, columns, placeholders)
        self.myCursor.execute(sql)
        self.mydb.commit()

def execute_insert():
    inserter = InsertDAO()
    messages = SSEClient('http://datagen.tigerdeals.svc.cluster.local:8080/streamTest')
    for msg in messages.resp_iterator:
        myDict = json.loads(msg.decode()[5:])
        columns = ', '.join(myDict.keys())
        placeholders = (", ".join(repr(e) for e in [str(v) for v in myDict.values()]))
        print(myDict)
        inserter.insert(columns, placeholders)


#execute_insert()


