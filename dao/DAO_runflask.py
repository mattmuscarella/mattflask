from flask import Flask, request
from flask_cors import CORS

from DAOSelect import SelectDAO
from DAOInsert import execute_insert

app = Flask(__name__)
CORS(app)

@app.route("/", methods=['POST', 'GET'])
def start_insert():
    execute_insert()
    return "start inserting"

@app.route("/curr_price", methods=['POST', 'GET'])
def get_curr_price():
    selector = SelectDAO()
    return str(selector.live_instrument_price())


@app.route("/avg", methods=['POST', 'GET'])
def get_avg ():
    a=''
    if request.args.to_dict()["type"] == "B":
        selector = SelectDAO()
        a = selector.avg_price_instrument_type(selector.table, "'B'")
        print(a)
    if request.args.to_dict()["type"] == "S":
        selector = SelectDAO()
        a = selector.avg_price_instrument_type(selector.table, "'S'")
        print(a)
    return a


@app.route("/realised", methods=['POST', 'GET'])
def get_realised():
    a=''
    if request.args.to_dict()["type"] == "B":
        selector = SelectDAO()
        a = selector.realized_profit("B")
        print(a)
    if request.args.to_dict()["type"] == "S":
        selector = SelectDAO()
        a = selector.realized_profit("S")
        print(a)
    return a

@app.route("/quantity", methods=['POST', 'GET'])
def get_quantity():
    a=''
    if request.args.to_dict()["type"] == "B":
        selector = SelectDAO()
        a = selector.quantity("B")
        print(a)
    if request.args.to_dict()["type"] == "S":
        selector = SelectDAO()
        a = selector.quantity("S")
        print(a)
    return a

@app.route("/check_connection", methods=['POST', 'GET'])
def check_connection():
    return "You are connected"

def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))
    


if __name__ == '__main__':
     bootapp()


