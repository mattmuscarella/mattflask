import mysql.connector
import json


class SelectDAO:
    def __init__(self):
        self.mydb = mysql.connector.connect(  # DB port
            host="mysql.tigerdeals.svc.cluster.local",
            user="root",
            passwd="ppp",
            database="deals")
        self.myCursor = self.mydb.cursor()

        self.table = 'deals.deal_data'
        self.table_user = 'deals.user'

    def avg_price_instrument_type(self, table, dealtype):  # username is dis
            sql = 'SELECT instrumentName, AVG(price) FROM {0} WHERE type={1} GROUP BY instrumentName'.format(table, dealtype)
            self.myCursor.execute(sql)
            return json.dumps(self.myCursor.fetchall())

    def realized_profit(self, dealtype):
        sql = 'select instrumentName, sum(price * quantity) from deals.deal_data where type = "{0}" group by instrumentName;'.format(dealtype)
        print(sql)
        self.myCursor.execute(sql)
        return json.dumps(self.myCursor.fetchall())

    def quantity(self, dealtype):
        sql = 'select instrumentName, sum(quantity) from deals.deal_data where type = "{0}" group by instrumentName;'.format(dealtype)
        self.myCursor.execute(sql)
        output = self.myCursor.fetchall()
        quantlist = [float(i[1]) for i in output]
        instlist = [i[0] for i in output]
        return json.dumps([list(a) for a in zip(instlist, quantlist)])

    def live_instrument_price_by_type(self, dealtype):
        sql = "(select instrumentName, price from deals.deal_data where instrumentName = 'Jupiter' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Koronis' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Deuteronic' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Galactia' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Floral' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Interstella' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Eclipse' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Astronomica' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Borealis' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Heliosphere' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Celestial' and type = '{0}' order by time DESC limit 1) union all (select instrumentName, price from deals.deal_data where instrumentName = 'Lunatic' and type = '{0}' order by time DESC limit 1);".format(dealtype)
        self.myCursor.execute(sql)
        return json.dumps(self.myCursor.fetchall())

    def live_instrument_price(self):
        buy = json.loads(self.live_instrument_price_by_type('B'))
        sell = json.loads(self.live_instrument_price_by_type('S'))
        buysell = []
        inst = []
        for i in range(len(buy)):
            buysell.append("{0} / {1}".format(buy[i][1], sell[i][1]))
            inst.append("{0}".format(buy[i][0]))
        output = sorted([list(a) for a in zip(inst, buysell)])
        return json.dumps(output)


def execute_select():
    selector = SelectDAO()
    return selector.live_instrument_price()

execute_select()