from flask import Flask, render_template, jsonify, request
from flask_cors import CORS, cross_origin

import json, requests

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'application/json'


@app.route("/login", methods=['POST', 'GET'])
@cross_origin()
def isUser():
    print("received request")
    authenticate = {}
    print(json.loads(request.data))
    authenticate['authenticated'] = 'true'
    return jsonify(authenticate)


@app.route("/curr_price", methods=['POST', 'GET'])
def current_price():
    response = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/curr_price",
                             params={'type': 'B'})
    labels = []
    data_buy_sell = []

    for elem in json.loads(response.text):
        print(elem)
        labels.append(elem[0])
        data_buy_sell.append(elem[1])
    return jsonify([labels, data_buy_sell])


@app.route("/dbconnect")
def connect_to_db():
    '''TODO try connection to database and return 'true' if connected 'false' if else '''
    connected_status = {}
    connected_status["connected"] = 'true'
    return jsonify(connected_status)


@app.route("/avgbuy", methods=['POST', 'GET'])
@cross_origin()
def get_average_buy_data():
    response = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/avg",
                                  params={'type': 'B'})
    labels = []
    data = []
    for elem in json.loads(response.text):
        labels.append(elem[0])
        data.append('{0:.2f}'.format(float(elem[1])))

    return jsonify([labels, data])



@app.route("/avgsell", methods=['POST', 'GET'])
@cross_origin()
def get_average_sell_data():
    response = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/avg",
                                  params={'type': 'S'})
    labels = []
    data = []
    for elem in json.loads(response.text):
        labels.append(elem[0])
        data.append('{0:.2f}'.format(float(elem[1])))

    return jsonify([labels, data])

@app.route('/check_connection')
@cross_origin()
def check_connection():
    response = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/check_connection")
    return str(response.text)

@app.route('/effective_pl')
@cross_origin()
def return_effective_profit_or_loss():
    response_buy = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/realised",
                                params={'type': 'B'})

    arr_buy = json.loads(response_buy.text)
    print(arr_buy)

    response_sell = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/realised",
                                 params={'type': 'S'})

    arr_sell = json.loads(response_sell.text)

    res = []
    for i in range(len(arr_buy)):
        res.append(float(arr_buy[i][1]) - float(arr_sell[i][1]))
        
    ef_pfofit = sum(res)

    response_buy = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/quantity",
                                params={'type': 'B'})

    arr_buy = json.loads(response_buy.text)

    response_sell = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/quantity",
                                 params={'type': 'S'})

    arr_sell = json.loads(response_sell.text)

    response_price = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/curr_price",
                            params={'type': 'B'})

    price_arr = json.loads(response_price.text)

    print(price_arr)
    curr_instrument_amount = []


    for i in range(len(arr_buy)):
        a = float(arr_buy[i][1]) - float(arr_sell[i][1])
        if a > 0:
            curr_instrument_amount.append(a)
        else:
            curr_instrument_amount.append(0)


    for i in range(len(curr_instrument_amount)):
        curr_instrument_amount[i] *= float(price_arr[i][1].split('/')[0])

    ef_pfofit += sum(curr_instrument_amount)

    return str('{0:.2f}'.format(ef_pfofit))

@app.route('/realized_pl')
@cross_origin()
def return_realized_profit_or_loss():
    response_buy = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/realised",
                            params={'type': 'B'})
    arr_buy = json.loads(response_buy.text)

    response_sell = requests.get("http://dao.tigerdeals.svc.cluster.local:8080/realised",
                            params={'type': 'S'})
    arr_sell = json.loads(response_sell.text)

    res = []
    for i in range(len(arr_buy)):
        res.append(float(arr_buy[i][1]) - float(arr_sell[i][1]))

    return jsonify('{0:.2f}'.format(sum(res)))


def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
     bootapp()
